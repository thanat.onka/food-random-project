import mongoose from 'mongoose'
import statusEnum from '../../../common/status.enum.js'

const foodSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  style: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  calories: {
    type: Number,
    required: true
  },
  status: {
    type: String,
    default: statusEnum.ACTIVE
  }
},{ timestamps:true })
const foodModel = mongoose.model('food',foodSchema)

export default foodModel