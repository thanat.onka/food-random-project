import express from 'express'
import FoodController from './controllers/food.controller.js'

const FoodRouter = express.Router()

FoodRouter.post('/food', FoodController.addFood)
FoodRouter.get('/food', FoodController.getFood)
FoodRouter.put('/food', FoodController.updateFood)
FoodRouter.delete('/food', FoodController.deleteFood)

export default FoodRouter