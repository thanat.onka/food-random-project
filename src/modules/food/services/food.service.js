import foodModel from '../models/food.schema.js'
import statusEnum from '../../../common/status.enum.js'

const FoodService = {
  addFood (data) {
    return new foodModel(data).save()
  },
  getFood (query) {
    return foodModel.find({ ...query, status: statusEnum.ACTIVE })
  },
  updateFood (id, data) {
    return foodModel.findByIdAndUpdate(id, data)
  },
  deleteFood (id) {
    return foodModel.findByIdAndUpdate(id, { status: statusEnum.DELETED })
  }
}

export default FoodService