import humps from 'humps'
import FoodService from '../services/food.service.js'

const FoodController = {
  async addFood (req, res) {
    try {
      const { name, style, image, calories } = humps.camelizeKeys(req.body)
      const getName = await FoodService.getFood({ name: name })
      if (getName[0] !== undefined) {
        console.log(getName[0].name)
        if (name === getName[0].name) {
          throw 'This food is already existed!'
        }
      }

      const added = await FoodService.addFood({ name, style, image, calories })

      res.json({
        data: added,
        success: true
      }).status(201)
    }
    catch(err) {
      console.error(`[FAILED TO ADD FOOD] >> ${err}`)
      res.json({
        message: `[FAILED TO ADD FOOD] >> ${err}`,
        success: false
      }).status(501)
    }
  },
  async getFood (req, res) {
    try {
      const { name, style, calories } = humps.camelizeKeys(req.query)
      const query = {
        name: name || { $ne: null },
        style: style || { $ne: null },
        calories: calories || { $ne: null }
      }
      const fetched = await FoodService.getFood(query)

      res.json({
        data: fetched,
        success: true
      }).status(200)
    }
    catch(err) {
      console.error(`[FAILED TO GET FOOD] >> ${err}`)
      res.json({
        message: `[FAILED TO GET FOOD] >> ${err}`,
        success: false
      }).status(501)
    }
  },
  async updateFood (req, res) {
    try {
      const { _id } = req.query
      const { name, style, image, calories } = humps.camelizeKeys(req.body)
      const updated = await FoodService.updateFood({ _id }, { name, style, image, calories })

      res.json({
        data: updated,
        success: true
      }).status(201)
    }
    catch(err) {
      console.error(`[FAILED TO UPDATE FOOD] >> ${err}`)
      res.json({
        message: `[FAILED TO UPDATE FOOD] >> ${err}`,
        success: false
      }).status(501)
    }
  },
  async deleteFood (req, res) {
    try {
      const { _id } = req.query
      const updated = await FoodService.deleteFood({ _id })

      res.json({
        data: updated,
        success: true
      }).status(201)
    }
    catch(err) {
      console.error(`[FAILED TO DELETE FOOD] >> ${err}`)
      res.json({
        message: `[FAILED TO DELETE FOOD] >> ${err}`,
        success: false
      }).status(501)
    }
  }
}

export default FoodController