import express from 'express'
import HistoryController from './controllers/history.controller.js'

const HistoryRouter = express.Router()

HistoryRouter.post('/history', HistoryController.addHistory)
HistoryRouter.get('/history', HistoryController.getHistory)
HistoryRouter.delete('/history', HistoryController.deleteHistory)

export default HistoryRouter
