import mongoose from 'mongoose'
import statusEnum from '../../../common/status.enum.js'

const historySchema = mongoose.Schema({
  foodName: {
    type: String,
    required: true
  },
  foodStyle: {
    type: String,
    required: true
  },
  historyTime: {
    type: String,
    required: true
  },
  status: {
    type: String,
    default: statusEnum.ACTIVE
  }
},{ timestamps:true })

export default mongoose.model('History',historySchema)