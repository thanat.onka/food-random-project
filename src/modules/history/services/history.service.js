import historySchema from '../models/history.schema.js'
import statusEnum from '../../../common/status.enum.js'

const HistoryService = {
  addHistory (data) {
    return new historySchema(data).save()
  },
  getHistory (query) {
    return historySchema.find({ ...query, status: statusEnum.ACTIVE })
  },
  deleteHistory (id) {
    return historySchema.findByIdAndUpdate(id, { status: statusEnum.DELETED })
  }
}

export default HistoryService