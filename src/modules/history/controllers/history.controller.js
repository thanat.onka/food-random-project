import humps from 'humps'
import HistoryService from '../services/history.service.js'

const HistoryController = {
  async addHistory (req, res) {
    try {
      const { foodName, foodStyle, historyTime } = humps.camelizeKeys(req.body)
      const added = await HistoryService.addHistory({ foodName, foodStyle, historyTime })

      res.json({
        data: added,
        success: true
      }).status(201)
    }
    catch(err) {
      console.error(`[FAILED TO ADD HISTORY] >> ${err}`)
      res.json({
        message: `[FAILED TO ADD HISTORY] >> ${err}`,
        success: false
      }).status(501)
    }
  },
  async getHistory (req, res) {
    try {
      const { foodName, foodStyle, historyTime } = humps.camelizeKeys(req.query)
      const query = {
        foodName: foodName || { $ne: null },
        foodStyle: foodStyle || { $ne: null },
        historyTime: historyTime || { $ne: null }
      }
      const fetched = await HistoryService.getHistory(query)

      res.json({
        data: fetched,
        success: true
      }).status(200)
    }
    catch(err) {
      console.error(`[FAILED TO GET HISTORY] >> ${err}`)
      res.json({
        message: `[FAILED TO GET HISTORY] >> ${err}`,
        success: false
      }).status(501)
    }
  },
  async deleteHistory (req, res) {
    try {
      const { _id } = req.query
      const updated = await HistoryService.deleteHistory({ _id })

      res.json({
        data: updated,
        success: true
      }).status(201)
    }
    catch(err) {
      console.error(`[FAILED TO DELETE HISTORY] >> ${err}`)
      res.json({
        message: `[FAILED TO DELETE HISTORY] >> ${err}`,
        success: false
      }).status(501)
    }
  }
}

export default HistoryController