import mongoose from 'mongoose'
import statusEnum from '../../../common/status.enum.js'

const customSchema = mongoose.Schema({
  spinName: {
    type: String,
    required: true
  },
  menu: {
    type: Array,
    required: true
  },
  status: {
    type: String,
    default: statusEnum.ACTIVE
  }
},{ timestamps: true })

export default mongoose.model('custom', customSchema)