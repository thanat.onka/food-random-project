import customSchema from '../models/custom.schema.js'
import statusEnum from '../../../common/status.enum.js'

const CustomService = {
  addCustom (data) {
    return new customSchema(data).save()
  },
  getCustom (query) {
    return customSchema.find({ ...query, status: statusEnum.ACTIVE })
  },
  updateCustom (id, data) {
    return customSchema.findByIdAndUpdate(id, data)
  },
  deleteCustom (id) {
    return customSchema.findByIdAndUpdate(id, { status: statusEnum.DELETED })
  }
}

export default CustomService