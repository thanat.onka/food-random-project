import humps from 'humps'
import CustomService from '../services/custom.service.js'

const CustomController = {
  async addCustom (req, res) {
    try {
      const { spinName, menu } = humps.camelizeKeys(req.body)
      const added = await CustomService.addCustom({ spinName, menu })

      res.json({
        data: added,
        success: true
      }).status(201)
    }
    catch(err) {
      console.log(`[FAILED TO ADD CUSTOM] >> ${err}`)
      req.json({
        message: `[FAILED TO ADD CUSTOM] >> ${err}`,
        success: false
      }).status(501)
    }
  },
  async getCustom (req, res) {
    try {
      const fecthed = await CustomService.getCustom()

      res.json({
        data: fecthed,
        success: true
      }).status(200)
    }
    catch(err) {
      console.log(`[FAILED TO GET CUSTOM] >> ${err}`)
      req.json({
        message: `[FAILED TO GET CUSTOM] >> ${err}`,
        success: false
      }).status(501)
    }
  },
  async updateCustom (req, res) {
    try {
      const { _id } = humps.camelizeKeys(req.query)
      const { spinName, menu } = humps.camelizeKeys(req.body)
      const updated = await CustomService.addCustom({ _id }, { spinName, menu })

      res.json({
        data: updated,
        success: true
      }).status(201)
    }
    catch(err) {
      console.log(`[FAILED TO UPDATE CUSTOM] >> ${err}`)
      req.json({
        message: `[FAILED TO UPDATE CUSTOM] >> ${err}`,
        success: false
      }).status(501)
    }
  },
  async deleteCustom (req, res) {
    try {
      const { _id } = humps.camelizeKeys(req.query)
      const deleted = await CustomService.addCustom({ _id })

      res.json({
        data: deleted,
        success: true
      }).status(201)
    }
    catch(err) {
      console.log(`[FAILED TO DELETE CUSTOM] >> ${err}`)
      req.json({
        message: `[FAILED TO DELETE CUSTOM] >> ${err}`,
        success: false
      }).status(501)
    }
  }
}

export default CustomController