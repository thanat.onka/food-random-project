import express from 'express'
import CustomController from './controllers/custom.controller.js'

const CustomRouter = express.Router()

CustomRouter.post('/custom', CustomController.addCustom)
CustomRouter.get('/custom', CustomController.getCustom)
CustomRouter.put('/custom', CustomController.updateCustom)
CustomRouter.delete('/custom', CustomController.deleteCustom)

export default CustomRouter