export default {
  ACTIVE: "Active",
  INACTIVE: "Inactive",
  DELETED: "Deleted"
}