import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import dotenv from 'dotenv'
import FoodRouter from './src/modules/food/food.route.js'
import HistoryRouter from './src/modules/history/history.route.js'
import CustomRouter from './src/modules/custom/custom.route.js'
dotenv.config()

const app = express()
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())

app.use('/api', FoodRouter)
app.use('/api', HistoryRouter)
app.use('/api', CustomRouter)

app.get('/',(req,res)=>{
  res.send('<h1>Food Random API</h1>')
})

mongoose.connect(process.env.DB_CONNECT,{ useNewUrlParser: true })

const PORT = process.env.PORT || 7070
app.listen(PORT, console.log(`Server is runnig... [PORT : ${ PORT }]`))